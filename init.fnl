(require-macros :macros)
(local {:application app :hotkey hotkey} hs)
(local map hs.fnutils.map)

(set hs.logger.defaultLogLevel "info")
(hs.loadSpoon :SpoonInstall)
(local install (. spoon :SpoonInstall))

; easy reloading via URL (e.g. `open hammerspoon://doreload`)
(do-invoke hs.urlevent.bind 
           (:doreload (fn [] (hs.reload))))

; Lunette: override some default keybindings
; (defaults https://github.com/scottwhudson/Lunette#default-keybindings
(let [hotkeys
      {:leftHalf    [[ [:cmd] :Left] ]
       :rightHalf   [[ [:cmd] :Right] ]
       :topHalf     [[ [:cmd] :up] ]
       :bottomHalf  [[ [:cmd] :down] ]
       :topLeft     [[ [:cmd :alt] :1] ]
       :topRight    [[ [:cmd :alt] :2] ]
       :bottomLeft  [[ [:cmd :alt] :3] ]
       :bottomRight [[ [:cmd :alt] :4] ]
       :nextDisplay [[ [:ctrl :alt :cmd] :right] ]
       :prevDisplay [[ [:ctrl :alt :cmd] :left] ]
       }
      ]
  (print "using lunette")
  (: install :andUse :Lunette)
  (: spoon.Lunette :bindHotkeys hotkeys))
  
(hotkey.bind [:cmd :ctrl] :t 
             "😾 terminal"
             (partial app.open "kitty"))

(hotkey.bind [:alt] :f4
             "hs: close window"
             (fn [] (-?> (or (hs.window.focusedWindow) nil) (: :close))))

(local dragger ((require :mod-drag-resize)))
(dragger.activate) 

(doto hs.hints (tset :showTitleThresh 10) (tset :style :vimperator))
(hotkey.bind [:cmd :alt] :tab
             (partial hs.hints.windowHints))
