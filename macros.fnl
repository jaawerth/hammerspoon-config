(fn do-invoke [func ...]
  (local to-invoke (list))
  (each [i invocation (ipairs [...])]
    (table.insert invocation 1 func)
    (table.insert to-invoke invocation))
  `(do @(table.unpack to-invoke)))

{:do-invoke do-invoke}
