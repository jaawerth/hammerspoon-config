(local default-action-flags {:move [:alt] :resize [:shift :alt]})

(fn init [tgt-flags]
  (local {:mouseEventDeltaX dx :mouseEventDeltaY dy} hs.eventtap.event.properties)
  
  (local win-filter
    (-> (hs.window.filter.copy hs.window.filter.default)
        (: :setOverrideFilter {:visible true :currentSpace true})
        (: :setSortOrder hs.window.filter.sortByFocusedLast)))

  (local find hs.fnutils.find)
  (fn window-under-mouse []
    (local screen (hs.mouse.getCurrentScreen))
    ; (local _ hs.application)
    (fn is-under-mouse [w]
      (and (= screen (: w :screen))
           (: (hs.geometry.new (hs.mouse.getAbsolutePosition))
              :inside (: w :frame))))

    (find (: win-filter :getWindows) is-under-mouse))

  (fn on [ev func]
    (hs.eventtap.new [(. hs.eventtap.event.types ev)] func))
 
  (local tgt-flags (or tgt-flags default-action-flags.move))
  (var tgt-window nil)
  (let
    [mover (on :leftMouseDragged
               (fn [e]
                 (when tgt-window
                   (: tgt-window :move
                      [(: e :getProperty dx) (: e :getProperty dy)]
                      nil false 0))))

     esc-cancel (on :keyDown
                    (fn [e]
                      (when (and tgt-window (= hs.keycodes.map.escape (: e :getKeyCode)))
                        (set tgt-window nil) (: mover :stop))
                      (values true [e])))

     down-handler (on :leftMouseDown
                      (fn [e]
                        (if (not (-?> (: e :getFlags) (: :containExactly tgt-flags)))
                          (values true [e])
                          (do (set tgt-window (window-under-mouse))
                            (-?> tgt-window (: :focus))
                            (: mover :start)))))

     up-handler (on :leftMouseUp
                    (fn [e]
                      (when tgt-window (: mover :stop) (set tgt-window nil))
                      (values true [e])))]

    {:activate (fn [] (: down-handler :start) (: esc-cancel :start) (: up-handler :start))
     :deactivate (fn [] (: mover :stop) (set tgt-window nil) (: up-handler :stop) (: down-handler :stop))}))
