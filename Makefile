Spoon_URL_Prefix := https://github.com/Hammerspoon/Spoons/raw/master/Spoons

Spoons/%.spoon: Spoon = $(notdir $@)
Spoons/%.spoon: Spoon_URL = $(Spoon_URL_Prefix)/$(Spoon).zip
#$(join $(Spoon_URL_Prefix), $(Spoon).zip)

Spoons/%.spoon:
	@ if [ -d $@ ]; then rm -r $@; fi
	@ mkdir -p $(dir $@) && cd $(dir $@)\
	  && echo "Downloading $(Spoon_URL)"\
	  && wget -cq -O $(Spoon).zip $(Spoon_URL)\
	  && unzip $(Spoon).zip && rm $(Spoon).zip
clean:
	@ rm -r Spoons/*
spoons: Spoons/SpoonInstall.spoon

.PHONY: spoons all build clean
