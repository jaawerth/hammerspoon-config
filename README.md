# My [hammerspoon](https://www.hammerspoon.org/) config

While hammerspoon's API is built around Lua, I am primarily using [fennel](https://fennel-lang.org/), a new Lisp that compiles to Lua with minimal overhead, because I like it :grin. The only Lua I'm using here other than fennel itself (in `lib/`) and the Lunette/SpoonInstall spoons is the minimal amount in [init.lua](init.lua) to kick off [init.fnl](init.fnl), which is where all my actual config is. This is currently using fennel 0.2.1-2

Apologies in advance if you use this and find quirks, or for messy code - I initially just started tracking this in git for my own use and then wrote this README up so I could share it with others more easily.

## Functionality

### alt+drag on any part of window to move!
Use `<alt>[drag]` to drag a window using any part of it (not just the title bar).

This is the one I actually put some time into via the [mod-drag-resize.fnl](mod-drag-resize.fnl) module that duplicates the alt+drag functionality of various Linux window managers. As with the other hotkeys, you can pass in an alternate shortcut in [init.fnl](init.fnl) to override my default. For example, to use `<cmd+shift>` as your modifier:
```clojure
(local dragger ((require :mod-drag-resize)
                  {:move [:cmd :shift]}))
(dragger.activate)
```
This is the one thing I wrote that isn't just using a pre-cooked capability; I was surprised not to find a full implementation of this functionality anywhere online (there may be a specialized app for it but I prefer to use general tools when I can).

### Lunette

I'm using the [Lunette](https://github.com/scottwhudson/Lunette) spoon with my own custom keybindings. You can set different ones in [init.fnl](init.fnl) if you so desire, or override defaults I haven't by passing the relevant valeus into the table (see the Lunette page for a table of options). This is a pretty neat bit of work that adds a bunch of the functionality that made me seek out hammerspoon in the first place, with things like:
- Shortcut (`<cmd>[arrow]` in my case) to snap a window to left/right half of screen
- Shortcut (`<cmd+alt>[1-4]`) to snap a window into quadrants
- Maximize without fullscreen (configured to `<cmd+alt>f`)
- Resize arbitrarily resize window with a hotkey
- Saves some of the window operations in a stack/buffer so you can undo (`<cmd+alt>z` here)
- More stuff I forgot to mention

### Hints overlay

Uses hammerspoon's [hs.hints](https://www.hammerspoon.org/docs/hs.hints.html) API to activate a convenient shortcut-based window switcher overlay with `<cmd+alt>tab`. This is at the bottom of [init.fnl](init.fnl) and you can change the hotkey there if desired. It also needs a little tweaking - in certain cases, since I'm letting it print the full window titles iwthout truncating, the titles of one window will overlap the hotkey to focus the other, which can get annoying. Still, it's a nice alternative to alt+tab!

### Custom shortcuts/functions

- Easily hammerspoon config via the URL hammerspoon://doreload (which can be invoked with `open` or via any tool that can GET the URL)
- Close the focused window with `alt+f4`
- Launch/focus the terminal (in my case, [kitty](https://sw.kovidgoyal.net/kitty/) with `<ctrl+cmd>t`

## Usage

- Clone this to $HOME/.hammerspoon and then run `make`, and it should automatically install the [SpoonInstall](https://www.hammerspoon.org/Spoons/SpoonInstall.html), which takes care of auto-installing any other [Spoons](https://www.hammerspoon.org/Spoons/), which are pre-made plugins to easily add functionality without writing a ton of code yourself.
```bash
git clone https://gitlab.com/jaawerth/hammerspoon-config.git $HOME/.hammerspoon &&\
  cd $HOME/.hammerspoon &&\
  make
```
- Install [hammerspoon](https://www.hammerspoon.org/) and launch it. It should prompt you, but you may need to enable accessibility in the settings to gain full functionality.

**NOTE:** Some of the hotkeys here may not work if they're utilized already by MacOS shortcuts - I believe I may have disabled a few of them. You can check the hammerspoon console for messages about this.
